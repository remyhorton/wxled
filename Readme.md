wxLED: LED display sequence editor
==================================
_wxLED_ is a program made for creating and editing the display sequences for LED displays.
Currently it is a technology preview the background of which is covered in an [article elsewhere][tech], but in the longer-term it is intended to provide the symbol and control sequences for a hardware display based on [LED display tiles][elec] I have made as electronics projects.
It is written in Python 3 and makes use of [wxPython Phoenix][wxpy].
Sequences can be loaded and saved from/to JSON files, and there is the ability to export sequence instructions in both source (i.e. text) and encoded binary format.

Note: wxLED has been discontinued in favour [Taispeaint][tais] which is rewrite that uses GTK/PYGobject.

## Screenshots
![Screenshot](screenshot1.png)
![Screenshot](screenshot2.png)
![Screenshot](screenshot3.png)


## Dependencies
At the moment _wxLED_ requires wxPython and the PyOpenGL modules which
can be installed using ```pip```:

```pip install -r requirements.txt```


## Licence
_wxLED_ is licenced under [version 3 of the GPL][gpl3].


## Contact
Send emails to ``remy.horton`` (at) ``gmail.com``

[wxpy]: https://wxpython.org/
[tech]: https://www.remy.org.uk/tech.php?tech=1558220400
[elec]: https://www.remy.org.uk/elec.php?elec=1543017600
[gpl3]: https://www.gnu.org/licenses/gpl.html
[tais]: https://bitbucket.org/remyhorton/taispeaint/
