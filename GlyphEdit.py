# GlyphEdit.py - Glyph editing dialog
#
# (C) Remy Horton, 2018-2019
# SPDX-License-Identifier: GPL-3.0-only

import wx
from Glyphs import Glyph, GlyphMenu


def setPensAndBrushes(objTarget):
        objTarget.penBG = wx.Pen( wx.Colour(150,150,150) )
        objTarget.brushBG = wx.Brush( wx.Colour(150,150,150) )
        objTarget.penOn = wx.Pen( wx.Colour(255,0,0) )
        objTarget.brushOn = wx.Brush( wx.Colour(255,0,0) )
        objTarget.penOff = wx.Pen( wx.Colour(155,100,100) )
        objTarget.brushOff = wx.Brush( wx.Colour(155,100,100) )
        objTarget.penSel = wx.Pen( wx.Colour(0,0,255) )
        objTarget.penUnsel = wx.Pen( wx.Colour(150,150,150) )


class GlyphDialog(wx.Dialog):
    def __init__(self):
        wx.Dialog.__init__(self)
        self.Bind(wx.EVT_WINDOW_CREATE, self.OnCreate)

    def OnCreate(self, event):
        self.Unbind(wx.EVT_WINDOW_CREATE)
        self.Bind(wx.EVT_SHOW, self.OnShow)
        self.ledPanel = wx.xrc.XRCCTRL(self,"ledFont",GlyphDisplay)
        self.ctrlLength = wx.xrc.XRCCTRL(self,"ctrlFontWidth",wx.SpinCtrl)
        self.ctrlHotkey = wx.xrc.XRCCTRL(self,"ctrlFontHotkey",wx.TextCtrl)
        self.ctrlScroll = wx.xrc.XRCCTRL(self,"ctrlFontScroll",wx.ScrolledWindow)
        self.btnNew = wx.xrc.XRCCTRL(self,"btnFontNew",wx.Button)
        self.btnNew.Bind(wx.EVT_BUTTON, self.evtButtonAdd)
        self.btnDel = wx.xrc.XRCCTRL(self,"btnFontDel",wx.Button)
        self.btnDel.Bind(wx.EVT_BUTTON, self.evtButtonDel)
        self.ctrlLength.Bind(wx.EVT_SPINCTRL, self.evtLenChange)
        self.ctrlHotkey.Bind(wx.EVT_TEXT, self.evtHotkeyChange)
        self.Bind(wx.EVT_CLOSE, self.evtClose)
        self.hasChanges = False

    def enableCtrls(self,isEnabled):
        self.ctrlLength.Enable(isEnabled)
        self.ctrlHotkey.Enable(isEnabled)
        self.btnDel.Enable(isEnabled)

    def getGlyph(self):
        return self.ledPanel.ptrGlyph

    def evtClose(self, event):
        if self.hasChanges:
            reply = wx.MessageBox(
                    "Save the changes made to the glyphs?",
                    "Save changes",
                    wx.YES_NO|wx.CANCEL|wx.CANCEL_DEFAULT|wx.CENTRE,
                    parent=self
                    )
            if reply == wx.CANCEL:
                return
        else:
            reply = wx.NO
        self.ctrlScroll.idxHighlight = None
        self.ledPanel.setGlyph( None )
        self.enableCtrls(False)
        self.EndModal(reply)
        self.hasChanges = False

    def evtLenChange(self, event):
        cntCols = event.GetPosition()
        self.ledPanel.setGlyphWidth(cntCols);
        self.getGlyph().setGlyphWidth(cntCols)
        self.Refresh()
        self.hasChanges = True

    def evtHotkeyChange(self, event):
        strNewKey = event.GetString()
        if len(strNewKey) == 0:
            self.getGlyph().hotkey = None
        else:
            self.getGlyph().hotkey = strNewKey[0]
        self.hasChanges = True

    def evtButtonAdd(self, event):
        self.glyphs.insert(0, Glyph(id=self.nextId))
        self.nextId += 1
        self.ctrlScroll.idxHighlight = 0
        self.ledPanel.setGlyph( 0 )
        self.enableCtrls(True)
        self.Refresh()
        self.ctrlScroll.calcHeight()
        self.ctrlScroll.Scroll(-1, 0)
        self.hasChanges = True

    def evtButtonDel(self, event):
        idxVictim = self.ctrlScroll.idxHighlight
        if idxVictim is None:
            return
        self.ctrlScroll.idxHighlight = None
        self.ledPanel.setGlyph( None )
        self.enableCtrls(False)
        del self.glyphs[idxVictim]
        self.Refresh()
        self.hasChanges = True

    def OnShow(self, event):
        self.ctrlScroll.calcHeight()

    def SetGlyphIndex(self,idxGlyph):
        self.ctrlScroll.SetHighlight(idxGlyph)
        if idxGlyph >= len(self.glyphs):
            idxGlyph = None
        self.ledPanel.setGlyph( idxGlyph )
        self.Refresh()
        if idxGlyph is None:
            self.enableCtrls(False)
        elif self.getGlyph().inUse:
            self.ctrlLength.Enable(True)
            self.ctrlHotkey.Enable(True)
            self.btnDel.Enable(False)
        else:
            self.enableCtrls(True)
        self.Refresh()


class GlyphDisplay(wx.Panel):
    def __init__(self):
        wx.Panel.__init__(self)
        self.Bind(wx.EVT_WINDOW_CREATE, self.OnCreate)

    def OnCreate(self, event):
        self.Unbind(wx.EVT_WINDOW_CREATE)
        setPensAndBrushes(self)
        self.lstCols = [0 for _ in range(0,15)]
        self.setGlyphWidth(5)
        self.Bind(wx.EVT_PAINT, self.evtPaint)
        self.Bind(wx.EVT_LEFT_DOWN, self.evtClick)
        self.ptrGlyph = None

    def setGlyph(self,idxGlyph):
        if idxGlyph is None:
            self.ptrGlyph = None
        else:
            self.ptrGlyph = self.GetParent().glyphs[idxGlyph]
            self.GetParent().ctrlLength.SetValue(self.ptrGlyph.width)
            self.setGlyphWidth(self.ptrGlyph.width)
            # Temporary unbind change callback so that programmatic change
            # of value does not trigger it. Oddly enough SpinCtrl does not
            # need to be disabled in the same way..
            self.GetParent().ctrlHotkey.Unbind(wx.EVT_TEXT)
            if self.ptrGlyph.hotkey is None:
                self.GetParent().ctrlHotkey.SetValue("")
            else:
                self.GetParent().ctrlHotkey.SetValue(self.ptrGlyph.hotkey)
            # Re-enable callback
            self.GetParent().ctrlHotkey.Bind(wx.EVT_TEXT, self.GetParent().evtHotkeyChange)
        self.Refresh()

    def setGlyphWidth(self, cntDots):
        self.cntDots = cntDots
        self.Refresh()

    def evtClick(self, event):
        if self.ptrGlyph is None:
            return
        (_,_,width,height) = self.GetClientRect()
        delta = (height // 22)
        row = ((event.GetY() // delta) - 1) // 3;
        col = ((event.GetX() // delta) - 1) // 3;
        self.ptrGlyph.toggleDot(col,row)
        self.GetParent().hasChanges = True
        self.GetParent().Refresh()

    def evtPaint(self, event=None):
        dc = wx.PaintDC(self);
        (_,_,width,height) = self.GetClientRect()
        delta = height // 22
        dc.Clear()
        step = delta * 3
        drawWidth = step * self.cntDots + delta
        drawOffset = (width // 2) - (drawWidth // 2)
        if False:
            dc.SetPen( self.penBG )
            dc.SetBrush( self.brushBG )
            dc.DrawRectangle(
                0,#drawOffset,
                0,
                drawWidth,
                height)
        if self.ptrGlyph is None:
            return
        for idxCol in range(0,self.cntDots):
            for idxRow in range(0,7):
                if self.ptrGlyph.isDotLit(idxCol,idxRow):
                    dc.SetPen( self.penOn )
                    dc.SetBrush( self.brushOn )
                else:
                    dc.SetPen( self.penOff )
                    dc.SetBrush( self.brushOff )
                dc.DrawCircle(
                    step * idxCol + delta * 2,
                    step * idxRow + delta * 2,
                    delta * 1
                    )


class GlyphList(GlyphMenu):
    def evtPaint(self, event):
        glyphs = self.GetParent().glyphs
        dc = wx.PaintDC(self);
        self.DoPrepareDC(dc)
        cntRows = 0
        cntCols = 0
        idxGlyph = 0
        for idxSlot in range(0,len(glyphs)):
            glyph = glyphs[idxGlyph]
            if self.idxGap is not None and self.idxGap != self.idxHighlight:
                if idxSlot == self.idxGap:
                    glyph = glyphs[self.idxHighlight]
                    if cntCols >= self.lenRow:
                        cntCols = cntCols - self.lenRow
                        cntRows += 1
                    self.drawGlyph(dc, cntCols, cntRows, glyph, True)
                    cntCols += 1
                    continue
                if idxGlyph == self.idxHighlight:
                    idxGlyph += 1
                    glyph = glyphs[idxGlyph]
                if cntCols >= self.lenRow:
                    cntCols = cntCols - self.lenRow
                    cntRows += 1
                self.drawGlyph(dc, cntCols, cntRows, glyph)
            else:
                if cntCols >= self.lenRow:
                    cntCols = cntCols - self.lenRow
                    cntRows += 1
                if self.idxHighlight is not None and self.idxHighlight == idxGlyph:
                    self.drawGlyph(dc, cntCols, cntRows, glyph, True)
                else:
                    self.drawGlyph(dc, cntCols, cntRows, glyph)
            cntCols += 1
            idxGlyph += 1

    def evtCreate(self, event):
        super(GlyphList,self).evtCreate(event)
        self.idxGap = None
        self.idxHighlight = None
        self.mouseDrag = False
        self.mouseDown = False
        self.mouseX = 0
        self.mouseY = 0
        self.Bind(wx.EVT_LEFT_DOWN, self.evtClick)
        self.Bind(wx.EVT_LEFT_UP, self.evtRelease)
        self.Bind(wx.EVT_MOTION, self.evtMoveMove)

    def evtRelease(self, event):
        if not self.mouseDown:
            return
        if self.mouseDrag:
            if self.idxGap is not None and self.idxGap != self.idxHighlight:
                #print(self.__class__,__name__,"Move tile",self.idxGap)
                glyphs = self.GetParent().glyphs
                ptrGlyph = glyphs[self.idxHighlight]
                del glyphs[self.idxHighlight]
                glyphs.insert(self.idxGap, ptrGlyph)
                self.GetParent().SetGlyphIndex( self.idxGap )
                self.GetParent().hasChanges = True
            self.idxGap = None
            self.Refresh()

        self.mouseDown = False
        self.mouseDrag = False

    def evtMoveMove(self, event):
        if not self.mouseDown:
            return
        posX,posY = self.CalcUnscrolledPosition(event.GetX(),event.GetY())
        if not self.mouseDrag:
            deltaX = self.mouseX - posX
            deltaY = self.mouseY - posY
            if deltaX > 10 or deltaX < -10 or deltaY > 10 or deltaY < -10:
                self.mouseDrag = True
        if self.mouseDrag:
            self.mouseX = posX
            self.mouseY = posY
            self.idxGap = self.calcClickIdx(event)
            self.Refresh()

    def evtClick(self, event):
        posX,posY = self.CalcUnscrolledPosition(event.GetX(),event.GetY())
        idxCol = posX // 70
        idxRow = posY // 40
        if idxCol >= self.lenRow:
            idxSelected = None
        else:
            idxSelected = idxRow * self.lenRow + idxCol
            self.GetParent().SetGlyphIndex( idxSelected )
            self.mouseDown = True
            self.isDragging = False
            self.mouseX = posX
            self.mouseY = posY

    def SetHighlight(self, idxHighlight):
        self.idxHighlight = idxHighlight

