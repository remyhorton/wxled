# Glyphs.py - Glyph handling & editing
#
# (C) Remy Horton, 2018-2019
# SPDX-License-Identifier: GPL-3.0-only

import wx


class Glyph(object):
    MaxWidth = 15
    def __init__(self, cols=None, hotkey=None, inUse=False, id=None):
        self.cols = [0 for _ in range(0,Glyph.MaxWidth)]
        self.width = 5
        if cols:
            for idxCol,valCol in enumerate(cols):
                self.cols[idxCol] = valCol
            self.width = len(cols)
        self.hotkey = hotkey
        self.inUse = inUse
        self.id = id

    def __str__(self):
        if self.hotkey:
            strKey = "Hotkey {0}".format(self.hotkey)
        else:
            strKey = "No hotkey"
        return "Glyph ({0}): {1}".format(
            strKey,
            " ".join([hex(x) for x in self.cols[0:self.width]])
            )

    def toggleDot(self,idxCol,idxRow):
        if idxCol < self.width:
            self.cols[idxCol] ^= 1 << idxRow

    def isDotLit(self,idxCol,idxRow):
        if self.cols[idxCol] & (1 << idxRow):
            return True
        return False

    def setGlyphWidth(self,cntCols):
        self.width = cntCols
        # Glyphs loaded from file have spare space truncated. Add it back in.
        if len(self.cols) < Glyph.MaxWidth:
            self.cols.extend( [0]*(Glyph.MaxWidth-len(self.cols)) )
        for idx in range(cntCols,Glyph.MaxWidth):
            self.cols[idx] = 0

    def getData(self):
        return [self.id, self.width, self.cols[:self.width], self.hotkey]

    def setData(self, data):
        self.id,self.width,self.cols,self.hotkey = data


class GlyphMenu(wx.ScrolledWindow):
    class BoundingBox(object):
        def __init__(self):
            self.padding = 5
            self.margin = 4
            self.dotpadding = 2
            self.dotmargin = 1
            self.dotsize = 3
            self.line = 2
            self.dotspacing = self.dotsize + self.dotmargin
            self.width   = self.dotspacing * 15 + self.dotpadding * 2
            self.height  = self.dotspacing * 7 + self.dotpadding * 2
            self.width  += 0
            self.height += 0

    def __init__(self):
        wx.ScrolledWindow.__init__(self)
        self.Bind(wx.EVT_WINDOW_CREATE, self.evtCreate)

    def evtCreate(self, event):
        self.Unbind(wx.EVT_WINDOW_CREATE)
        self.Bind(wx.EVT_PAINT, self.evtPaint)
        self.Bind(wx.EVT_SIZE, self.evtResize)
        self.ShowScrollbars(wx.SHOW_SB_NEVER, wx.SHOW_SB_ALWAYS)
        # Drawing stuff
        self.brushBG = wx.Brush( wx.Colour(150,150,150) )
        self.brushPx = wx.Brush( wx.Colour(255,0,0))
        self.penBG = wx.Pen( wx.Colour(150,150,150), width=2 )
        self.penPx = wx.Pen( wx.Colour(0,0,255), width=2 )
        self.bbox = self.BoundingBox()
        self.calcHeight()

    def evtResize(self, event):
        self.calcHeight()
        self.Refresh()
        event.Skip()

    def evtPaint(self, event):
        glyphs = self.GetParent().glyphs
        dc = wx.PaintDC(self);
        self.DoPrepareDC(dc)
        cntRows = 0
        cntCols = 0
        for glyph in glyphs:
            if cntCols == self.lenRow:
                cntCols = 0
                cntRows += 1
            self.drawGlyph(dc, cntCols, cntRows, glyph)
            cntCols += 1

    def drawGlyph(self, dc, idxX, idxY, glyph, highlight=False):
        bbox = self.bbox
        posX = bbox.margin + (bbox.padding + bbox.width) * idxX
        posY = bbox.margin + (bbox.padding + bbox.height) * idxY
        # Using self.penBG that has same colour as self.brushBG
        # rather than wx.NullPen means not having to make allowance
        # for the latter's thickness of zero.
        if highlight:
            dc.SetPen( self.penPx)
        else:
            dc.SetPen( self.penBG )
        dc.SetBrush( self.brushBG )
        dc.DrawRectangle(posX, posY, bbox.width, bbox.height)
        dc.SetPen( wx.NullPen )
        dc.SetBrush( self.brushPx )
        for idxCol in range(0,glyph.width):
            x = posX + bbox.dotpadding + idxCol * bbox.dotspacing
            bits = glyph.cols[idxCol]
            for idxBit in range(0,7):
                if bits & (1 << idxBit):
                    y = posY + bbox.dotpadding + idxBit * bbox.dotspacing
                    dc.DrawRectangle(x, y, bbox.dotsize, bbox.dotsize)

    def calcHeight(self):
        glyphs = self.GetParent().glyphs
        bbox = self.bbox
        self.lenRow = \
            (self.Size.width - bbox.margin*2) // (bbox.width + bbox.padding)
        countRows = len(glyphs) // self.lenRow
        if len(glyphs) % self.lenRow != 0 and False:
            # Partly filled row
            countRows += 1

        totalHeight = bbox.margin*2 + (bbox.height + bbox.padding)*countRows
        self.SetVirtualSize( wx.Size(self.Size.width, totalHeight ) )
        self.SetScrollRate(1, bbox.height + bbox.padding)

    def calcClickIdx(self, event):
        bbox = self.bbox
        posX,posY = self.CalcUnscrolledPosition(event.GetX(),event.GetY())
        if posX < bbox.margin or posX > self.Size.width - bbox.margin:
            return None
        if posY < bbox.margin:
            return None
        posX -= bbox.margin
        posY -= bbox.margin
        idxCol = int(posX // (bbox.width + bbox.padding ))
        idxRow = int(posY // (bbox.height + bbox.padding ))
        posX -= (bbox.width + bbox.padding ) * idxCol
        posY -= (bbox.height + bbox.padding ) * idxRow
        if posY >= bbox.height or posX >= bbox.width:
            return None
        return idxRow * self.lenRow + idxCol


