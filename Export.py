# Export.py - :CD sequence export routines
#
# (C) Remy Horton, 2018-2019
# SPDX-License-Identifier: GPL-3.0-only


def int2bool(bite):
    mask = 1
    bools = []
    while mask <= 0x80:
        if bite & mask:
            bools.append( True )
        else:
            bools.append( False )
        mask = mask << 1
    return bools

def bool2int(listBools):
    mask = 1
    accum = 0
    for bit in listBools:
        if bit:
            accum = accum | mask
        mask = mask << 1
    return accum


class ExporterAsm(object):
    def __init__(self):
        self.listInstructions = []

    def instLoad(self, count):
        self.listInstructions.append("Load {0:02x}".format(count))

    def instDone(self):
        self.listInstructions.append("Done")

    def instData(self, byte):
        self.listInstructions.append("Data {0:02x}".format(byte))

    def instRise(self, listBits):
        listBytes = []
        for offset in range(0,len(listBits),8):
            nibble = listBits[offset:offset+8]
            listBytes.append( bool2int(nibble) )
        self.listInstructions.append("ShUp {0}".format(listBytes))

    def instFall(self, listBits):
        listBytes = []
        for offset in range(0,len(listBits),8):
            nibble = listBits[offset:offset+8]
            listBytes.append( bool2int(nibble) )
        self.listInstructions.append("ShDn {0}".format(listBytes))

    def instWait(self, delay):
        self.listInstructions.append("Wait {0}".format(delay))

    def write(self, fpOutput):
        for inst in self.listInstructions:
            fpOutput.write( inst.encode('utf-8') )
            fpOutput.write(b'\n')




class ExporterBin(object):
    def __init__(self):
        self.listInstructions = []

    def instLoad(self, count):
        self.listInstructions.append( 0b11111101 )

    def instDone(self):
        self.listInstructions.append( 0b11111110 )

    def instData(self, byte):
        self.listInstructions.append( 0x7f & byte )

    def _instVertical(self, listBits, opcode):
        for offset in range(0,len(listBits),5):
            nibble = listBits[offset:offset+5]
            self.listInstructions.append( opcode | bool2int(nibble) )
        self.listInstructions.append( 0b11111110 )

    def instRise(self, bites):
        self._instVertical(bites, 0b10100000)

    def instFall(self, bites):
        self._instVertical(bites, 0b10000000)

    def instWait(self, delay):
        self.listInstructions.append( 0b11000000 | delay )

    def write(self, fpOutput):
        for inst in self.listInstructions:
            bite = inst.to_bytes(length=1, byteorder='little')
            fpOutput.write( bite )

