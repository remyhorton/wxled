# SequenceEdit.py - Display sequence editing
#
# (C) Remy Horton, 2018-2019
# SPDX-License-Identifier: GPL-3.0-only

import wx
from GlyphEdit import setPensAndBrushes
from Glyphs import GlyphMenu

class EditDialog(wx.Dialog):
    def __init__(self):
        wx.Dialog.__init__(self)
        self.Bind(wx.EVT_WINDOW_CREATE, self.OnCreate)

    def OnCreate(self, event):
        self.Unbind(wx.EVT_WINDOW_CREATE)
        self.Bind(wx.EVT_SHOW, self.evtShow)
        self.ledPanel = wx.xrc.XRCCTRL(self,"ledEdit",EditLED)
        self.ctrlPreview = wx.xrc.XRCCTRL(self,"ctrlEditPreview",EditPreview)
        self.Bind(wx.EVT_SHOW, self.evtShow)
        self.Bind(wx.EVT_CHAR, self.evtKeyboard)
        self.Bind(wx.EVT_CLOSE, self.evtClose)
        self.hasChanges = False

    def evtShow(self, event=None):
        self.lookup = dict()
        for glyph in self.glyphs:
            if glyph.hotkey is not None:
                self.lookup[glyph.hotkey] = glyph.id
        self.ctrlPreview.calcWidth()
        self.SelectSymbol( len(self.text) - 1)
        self.Refresh()

    def evtKeyboard(self, event):
        code = event.GetUnicodeKey()
        if code == wx.WXK_NONE:
            code = event.GetKeyCode()
            if code in [wx.WXK_LEFT,wx.WXK_NUMPAD_LEFT]:
                if self.ledPanel.idxCursor > 0:
                    self.ledPanel.idxCursor -= 1
            elif code in [wx.WXK_RIGHT,wx.WXK_NUMPAD_RIGHT]:
                if self.ledPanel.idxCursor < len(self.text):
                    self.ledPanel.idxCursor += 1
            elif code == wx.WXK_DELETE:
                self.DeleteNextSymbol()
            else:
                return
        # Backspace is also a valid character
        elif code == wx.WXK_BACK:
            self.DeleteLastSymbol()
        else:
            try:
                self.InsertSymbol(self.lookup[chr(code)])
            except KeyError:
                return
        self.ledPanel.Refresh()

    def evtClose(self, event):
        if self.hasChanges:
            reply = wx.MessageBox(
                    "Keep changes made to the sequence?",
                    "Save changes?",
                    wx.YES_NO|wx.CANCEL|wx.CANCEL_DEFAULT|wx.CENTRE,
                    parent=self
                    )
            if reply == wx.CANCEL:
                return
        else:
            reply = wx.NO
        self.hasChanges = False
        self.EndModal(reply)

    def SelectSymbol(self,idxText):
        self.ledPanel.idxCursor = idxText + 1
        self.ledPanel.Refresh()

    def InsertSymbol(self, idGlyph):
        ptrGlyph = self.glyphLookup[idGlyph]
        self.text.insert(self.ledPanel.idxCursor, ptrGlyph.id)
        self.ledPanel.idxCursor += 1
        self.ctrlPreview.calcWidth()
        self.Refresh()
        self.hasChanges = True

    def DeleteNextSymbol(self):
        idxVictim = self.ledPanel.idxCursor
        if idxVictim >= len(self.text):
            return
        del self.text[self.ledPanel.idxCursor]
        self.ctrlPreview.calcWidth()
        self.Refresh()
        self.hasChanges = True

    def DeleteLastSymbol(self):
        idxVictim = self.ledPanel.idxCursor
        if idxVictim < 1:
            # No left-hand symbol to delete
            return
        del self.text[self.ledPanel.idxCursor - 1]
        self.ledPanel.idxCursor += -1
        self.ctrlPreview.calcWidth()
        self.Refresh()
        self.hasChanges = True


class EditLED(wx.Panel):
    def __init__(self):
        wx.Panel.__init__(self)
        self.Bind(wx.EVT_WINDOW_CREATE, self.evtCreate)

    def evtCreate(self, event):
        self.Unbind(wx.EVT_WINDOW_CREATE)
        setPensAndBrushes(self)
        self.Bind(wx.EVT_PAINT, self.evtPaint)
        self.idxCursor = 0
        self.Bind(wx.EVT_CHAR, self.evtKeyboard)

    def evtPaint(self, event):
        (_,_,width,height) = self.GetClientRect()
        dc = wx.PaintDC(self);
        dc.Clear()
        dc.SetPen( self.penSel )
        dc.DrawLine(width // 2 - 1, 0, width // 2 - 1, height)
        dc.DrawLine(width // 2,     0, width // 2,     height)
        dc.DrawLine(width // 2 + 1, 0, width // 2 + 1, height)

        delta = height // 22
        step = delta * 3
        posX = width // 2 - 2
        for idGlyph in self.GetParent().text[self.idxCursor:]:
            glyph = self.GetParent().glyphLookup[idGlyph]
            for idxCol in range(0, glyph.width):
                for idxRow in range(0,7):
                    if glyph.isDotLit(idxCol,idxRow):
                        dc.SetPen( self.penOn )
                        dc.SetBrush( self.brushOn )
                    else:
                        dc.SetPen( self.penOff )
                        dc.SetBrush( self.brushOff )
                    dc.DrawCircle(
                        posX + step * idxCol + delta * 2,
                        step * idxRow + delta * 2,
                        delta * 1
                        )
            posX += step * glyph.width
            if posX > width:
                break

        posX = width // 2 - 3
        for idGlyph in reversed(self.GetParent().text[:self.idxCursor]):
            glyph = self.GetParent().glyphLookup[idGlyph]
            posX -= step * glyph.width
            for idxCol in range(0, glyph.width):
                for idxRow in range(0,7):
                    if glyph.isDotLit(idxCol,idxRow):
                        dc.SetPen( self.penOn )
                        dc.SetBrush( self.brushOn )
                    else:
                        dc.SetPen( self.penOff )
                        dc.SetBrush( self.brushOff )
                    dc.DrawCircle(
                        posX + step * idxCol + delta * 2,
                        step * idxRow + delta * 2,
                        delta * 1
                        )
            if posX < 0:
                break

    def evtKeyboard(self, event):
        self.GetParent().GetEventHandler().AddPendingEvent(event)


class EditPreview(wx.ScrolledWindow):
    def __init__(self):
        wx.ScrolledWindow.__init__(self)
        self.Bind(wx.EVT_WINDOW_CREATE, self.OnCreate)

    def OnCreate(self, event):
        self.Unbind(wx.EVT_WINDOW_CREATE)
        setPensAndBrushes(self)
        self.Bind(wx.EVT_PAINT, self.evtPaint)
        self.Bind(wx.EVT_LEFT_DOWN, self.evtClick)
        self.Bind(wx.EVT_CHAR, self.evtKeyboard)
        setPensAndBrushes(self)
        self.ShowScrollbars(wx.SHOW_SB_ALWAYS, wx.SHOW_SB_NEVER)

    def calcWidth(self):
        lenStep = 4
        totalLength = 0
        for idxSymbol in self.GetParent().text:
            glyph = self.GetParent().glyphs[idxSymbol]
            totalLength += lenStep * glyph.width
        self.SetVirtualSize( wx.Size(totalLength,self.Size.height) )
        self.SetScrollRate(10, 0)
        self.Refresh()

    def evtPaint(self, event=None):
        dc = wx.PaintDC(self);
        self.DoPrepareDC(dc)
        space = 1
        radius = 3
        dc.SetPen( self.penOn )
        dc.SetBrush( self.brushOn )
        cntCols = 0
        for idSymbol in self.GetParent().text:
            glyph = self.GetParent().glyphLookup[idSymbol]
            for idxCol in range(0,glyph.width):
                for idxRow in range(0,7):
                    if glyph.cols[idxCol] & (1 << idxRow):
                        dc.DrawRectangle(
                        space*2 + (radius+space)*cntCols + 3,
                        space*2 + (radius+space)*idxRow + 3,
                        radius, radius)
                cntCols += 1

    def evtClick(self, event):
        posX = self.GetViewStart()[0] * 5 + event.GetX()
        lenStep = 4
        idxClick = 0
        totalLength = 0
        for idxSymbol in self.GetParent().text:
            glyph = self.GetParent().glyphs[idxSymbol]
            width = lenStep * glyph.width
            if posX < totalLength + width:
                self.GetParent().SelectSymbol(idxClick)
                return
            totalLength += width
            idxClick += 1

    def evtKeyboard(self, event):
        self.GetParent().GetEventHandler().AddPendingEvent(event)


class EditKeyboard(GlyphMenu):
    def evtCreate(self, event):
        self.Bind(wx.EVT_CHAR, self.evtKeyboard)
        self.Bind(wx.EVT_LEFT_DOWN, self.evtLeftClick)
        super(EditKeyboard,self).evtCreate(event)

    def evtKeyboard(self, event):
        self.GetParent().GetEventHandler().AddPendingEvent(event)

    def evtLeftClick(self, event):
        dialog = self.GetParent()
        idxGlyph = self.calcClickIdx(event)
        if idxGlyph is None:
            return
        ptrGlyph = dialog.glyphs[idxGlyph]
        dialog.InsertSymbol(ptrGlyph.id)

