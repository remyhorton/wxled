#!/usr/bin/env python3
# wxLED.py - Main program entrypoint
#
# (C) Remy Horton, 2018-2019,2023
# SPDX-License-Identifier: GPL-3.0-only


import wx
import wx.xrc
import wx.glcanvas
import json
import re
import OpenGL.GL as gl
import math
import copy

from Glyphs import Glyph
from Export import ExporterAsm, ExporterBin

class Sequence(object):
    def __init__(self, text=[]):
        self.entry_mode = 0
        self.entry_wait = 0
        self.exit_hold = 0
        self.exit_mode = 0
        self.exit_wait = 0
        self.text = text
        self.calcUseSet()

    def copyText(self):
        return copy.copy(self.text)

    def setText(self, newText):
        self.text = newText
        self.calcUseSet()

    def calcUseSet(self):
        self.used = set()
        for idVal in self.text:
            self.used.add(idVal)

    def getBits(self, glyphs):
        listCols = []
        for letter in self.text:
            glyph = glyphs[letter]
            listCols.extend( glyph.cols[:glyph.width] )
        return listCols

    def getData(self):
        return [
            self.text,
            self.entry_mode,
            self.entry_wait,
            self.exit_hold,
            self.exit_mode,
            self.exit_wait
            ]

    def setData(self, data):
        self.text = data[0]
        self.entry_mode = data[1]
        self.entry_wait = data[2]
        self.exit_hold = data[3]
        self.exit_mode = data[4]
        self.exit_wait = data[5]


class MainDisplay(wx.glcanvas.GLCanvas):
    def __init__(self, parent):
        wx.glcanvas.GLCanvas.__init__(
                self, parent, wx.ID_ANY, None, wx.DefaultPosition,
                wx.Size(-1, 132), 0
                )
        self.app = parent.app
        self.contGL = wx.glcanvas.GLContext(self)
        self.pixels = []
        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_PAINT, self.OnRedraw)
        self.Bind(wx.EVT_TIMER, self.evtTimer)
        self.timer = wx.Timer(self, wx.ID_ANY)

    def evtTimer(self, event):
        seq = self.ptrSeq

        # Init
        if self.state == 0:
            if self.ptrSeq.entry_mode == 0:
                idxPix = 0
                idxLED = 0
                while idxLED < self.numCols and idxPix < len(self.listCols):
                    self.pixels[idxLED] = self.listCols[idxPix]
                    idxLED += 1
                    idxPix += 1
                while idxLED < self.numCols:
                    self.pixels[idxLED] = 0
                    idxLED += 1
                self.Refresh()
                self.state = 20
                self.offset = 0
                self.timer.StartOnce(1000 * seq.entry_wait)
                return
            elif self.ptrSeq.entry_mode == 1:
                self.state = 11
                self.offset = 7
            elif self.ptrSeq.entry_mode == 2:
                self.state = 12
                self.offset = 7
            elif self.ptrSeq.entry_mode == 3:
                self.state = 13
                self.offset = self.numCols

        # Entry
        if self.state == 11 or self.state == 12:
            idxPix = 0
            idxLED = 0
            while idxLED < self.numCols and idxPix < len(self.listCols):
                if self.state == 11:
                    self.pixels[idxLED] = self.listCols[idxPix] << (self.offset)
                else:
                    self.pixels[idxLED] = self.listCols[idxPix] >> self.offset
                idxLED += 1
                idxPix += 1
            while idxLED < self.numCols:
                self.pixels[idxLED] = 0
                idxLED += 1
            self.Refresh()
            if self.offset == 0:
                self.state = 20
                self.offset = 0
                self.timer.StartOnce(1000 * seq.entry_wait)
                return
            else:
                self.timer.StartOnce(40)
            self.offset -= 1
        elif self.state == 13:
            if self.offset == -1:
                self.state = 20
                self.offset = 0
                self.timer.StartOnce(1000 * seq.entry_wait)
                return
            idxLED = 0
            while idxLED < self.offset:
                self.pixels[idxLED] = 0
                idxLED += 1
            idxPix = 0
            while idxLED < self.numCols and idxPix < len(self.listCols):
                self.pixels[idxLED] = self.listCols[idxPix]
                idxLED += 1
                idxPix += 1
            while idxLED < self.numCols:
                self.pixels[idxLED] = 0
                idxLED += 1
            self.Refresh()
            self.offset -= 1
            self.timer.StartOnce(40)

        # Scroll to end
        elif self.state == 20:
            idxPix = self.offset
            idxLED = 0
            while idxLED < self.numCols and idxPix < len(self.listCols):
                self.pixels[idxLED] = self.listCols[idxPix]
                idxLED += 1
                idxPix += 1
            while idxLED < self.numCols:
                self.pixels[idxLED] = 0
                idxLED += 1
            self.Refresh()
            if self.offset >= len(self.listCols) - self.numCols - 1:
                if seq.exit_mode == 0:
                    self.state = 50
                elif seq.exit_mode == 1:
                    self.state = 31
                    self.offset2 = self.offset
                    self.offset = 0
                elif seq.exit_mode == 2:
                    self.state = 32
                    self.offset2 = self.offset
                    self.offset = 0
                elif seq.exit_mode == 3:
                    self.state = 30
                self.timer.StartOnce(1000 * seq.exit_hold)
                return
            else:
                self.timer.StartOnce(40)
            self.offset += 1

        # Exit
        elif self.state == 30:
            idxPix = self.offset
            idxLED = 0
            while idxLED < self.numCols and idxPix < len(self.listCols):
                self.pixels[idxLED] = self.listCols[idxPix]
                idxLED += 1
                idxPix += 1
            while idxLED < self.numCols:
                self.pixels[idxLED] = 0
                idxLED += 1
            self.Refresh()
            if self.offset >= len(self.listCols):
                self.state = 40
                #self.timer.StartOnce(1000 * seq.entry_wait)
            else:
                self.timer.StartOnce(40)
            self.offset += 1
        if self.state == 31 or self.state == 32:
            idxPix = self.offset2
            idxLED = 0
            while idxLED < self.numCols and idxPix < len(self.listCols):
                if self.state == 31:
                    self.pixels[idxLED] = self.listCols[idxPix] >> (self.offset)
                else:
                    self.pixels[idxLED] = self.listCols[idxPix] << (self.offset)
                idxLED += 1
                idxPix += 1
            while idxLED < self.numCols:
                self.pixels[idxLED] = 0
                idxLED += 1
            self.Refresh()
            if self.offset == 7:
                self.state = 50
                self.offset = 0
                self.timer.StartOnce(1000 * seq.entry_wait)
                return
            else:
                self.timer.StartOnce(40)
            self.offset += 1

    def OnSize(self, event):
        # If the OpenGL stuff is not done via a deferred event, then there
        # will be the following error:
        #     wx._core.wxAssertionError: C++ assertion "xid" failed at
        #     ./glx11.cpp(194) in SetCurrent(): window must be shown
        wx.CallAfter(self.SetView)
        #self.SetView()
        event.Skip()

    def SetView(self):
        size = self.GetClientSize()
        self.numCols = size.width // 18
        self.SetCurrent(self.contGL)
        gl.glViewport(0, 0, size.width, size.height)
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        gl.glOrtho(0.0, size.width, 0.0, size.height, 0.0, -3.0)
        gl.glMatrixMode(gl.GL_MODELVIEW)
        if len(self.pixels) < self.numCols:
            self.pixels.extend( [0x00] * (self.numCols - len(self.pixels)) )

    def OnRedraw(self, event):
        dc = wx.PaintDC(self)
        self.SetCurrent(self.contGL)
        gl.glClearColor( 0.7, 0.7, 0.7, 1.0)
        gl.glClear( gl.GL_COLOR_BUFFER_BIT )
        gl.glLoadIdentity()
        gl.glTranslatef(-6.0, 0.0, 0.0)
        for idx in range(0, self.numCols):
            gl.glTranslatef(18.0, 0.0, 0.0)
            self.plotColumn( self.pixels[idx] )
        self.SwapBuffers()

    def plotColumn(self, pixels):
        radius = 6.0
        gl.glPushMatrix()
        gl.glTranslatef(00.0, 120.0, 0.0)
        for idx in range(0,7):
            self.plotCircle(radius, pixels >> idx)
            gl.glTranslatef(00.0, -18.0, 0.0)
        gl.glPopMatrix()

    def plotCircle(self, radius, colour):
        if colour & 0x01:
            gl.glColor3f(0.9, 0.0, 0.0)
        else:
            gl.glColor3f(0.9, 0.7, 0.7)
        gl.glBegin(gl.GL_TRIANGLE_FAN)
        gl.glVertex3f(0, 0, 1.0)
        angle = math.pi / 8.0
        for step in range(0, 16+1):
            gl.glVertex3f(
                    radius * math.sin(angle * step),
                    radius * math.cos(angle * step),
                    1.0)
        gl.glEnd()


    def clear(self):
        self.timer.Stop()
        for idx in range(0,len(self.pixels)):
            self.pixels[idx] = 0
        self.Refresh()

    def playSequence(self, seq):
        self.ptrSeq = seq
        self.listCols = seq.getBits(self.app.glyphLookup)
        self.state = 0
        self.timer.StartOnce(1)



class SequenceProperties(wx.Panel):
    def __init__(self):
        wx.Panel.__init__(self)
        self.Bind(wx.EVT_WINDOW_CREATE, self.evtCreate)

    def evtCreate(self, event):
        self.Unbind(wx.EVT_WINDOW_CREATE)
        self.ctrlEntry = wx.xrc.XRCCTRL(self,"ctrlPropEntry",wx.Choice)
        self.ctrlEntry.Append('Show immediatly')
        self.ctrlEntry.Append('Up from below')
        self.ctrlEntry.Append('Down from above')
        self.ctrlEntry.Append('In from right')
        self.ctrlEntry.SetSelection(0)
        self.ctrlEntryWait = wx.xrc.XRCCTRL(self,"ctrlPropDelay1",wx.SpinCtrl)
        self.ctrlExitHold = wx.xrc.XRCCTRL(self,"ctrlPropDelay2",wx.SpinCtrl)
        self.ctrlExit = wx.xrc.XRCCTRL(self,"ctrlPropExit",wx.Choice)
        self.ctrlExit.Append('Stop on end')
        self.ctrlExit.Append('Rise up')
        self.ctrlExit.Append('Fall off')
        self.ctrlExit.Append('Scroll off')
        self.ctrlExit.SetSelection(0)
        self.ctrlExitWait = wx.xrc.XRCCTRL(self,"ctrlPropWait",wx.SpinCtrl)
        self.activeSeq = None

    def syncSeqWithCtrls(self):
        if self.activeSeq is not None:
            self.activeSeq.entry_mode = self.ctrlEntry.GetSelection()
            self.activeSeq.entry_wait = self.ctrlEntryWait.GetValue()
            self.activeSeq.exit_hold = self.ctrlExitHold.GetValue()
            self.activeSeq.exit_mode = self.ctrlExit.GetSelection()
            self.activeSeq.exit_wait = self.ctrlExitWait.GetValue()

    def setActiveSequence(self, newSelection):
        if self.activeSeq is not None:
            self.syncSeqWithCtrls()
            ctrlState = [
                self.activeSeq.entry_mode,
                self.activeSeq.entry_wait,
                self.activeSeq.exit_hold,
                self.activeSeq.exit_mode,
                self.activeSeq.exit_wait
                ]
            if self.ctrlState != ctrlState:
                self.app.hasChanges = True
            self.activeSeq = None
            self.app.pnlLED.clear()

        if newSelection is None:
            self.Enable(False)
        else:
            self.ctrlEntry.SetSelection(newSelection.entry_mode)
            self.ctrlEntryWait.SetValue(newSelection.entry_wait)
            self.ctrlExitHold.SetValue(newSelection.exit_hold)
            self.ctrlExit.SetSelection(newSelection.exit_mode)
            self.ctrlExitWait.SetValue(newSelection.exit_wait)
            self.ctrlState = [
                newSelection.entry_mode,
                newSelection.entry_wait,
                newSelection.exit_hold,
                newSelection.exit_mode,
                newSelection.exit_wait
                ]
            self.activeSeq = newSelection
            self.app.pnlLED.playSequence(newSelection)
            self.Enable(True)


class SequenceList(wx.ScrolledWindow):
    def __init__(self, parent):
        wx.ScrolledWindow.__init__(
            self, parent ,wx.ID_ANY,
            wx.DefaultPosition, wx.DefaultSize, wx.VSCROLL
            )
        self.SetScrollRate(1, 20)
        self.ShowScrollbars(wx.SHOW_SB_NEVER,wx.SHOW_SB_ALWAYS)
        self.app = parent.app
        self.SetBackgroundColour( \
            wx.SystemSettings.GetColour( wx.SYS_COLOUR_HIGHLIGHTTEXT )
            )
        # Drawing stuff
        self.brushBG = wx.Brush( wx.Colour(150,150,150) )
        self.brushPx = wx.Brush( wx.Colour(255,0,0))
        self.penPx = wx.Pen( wx.Colour(0,0,255), width=2 )
        # Popup menu init
        self.idPopup = wx.NewIdRef(4)
        self.Bind(wx.EVT_MENU, self.evtPopupUp,   id=self.idPopup[0]);
        self.Bind(wx.EVT_MENU, self.evtPopupDown, id=self.idPopup[1]);
        self.Bind(wx.EVT_MENU, self.evtPopupEdit, id=self.idPopup[2]);
        self.Bind(wx.EVT_MENU, self.evtPopupDel,  id=self.idPopup[3]);
        self.menuPopup = wx.Menu()
        self.menuPopup.Append(self.idPopup[0], "Move up")
        self.menuPopup.Append(self.idPopup[1], "Move down")
        self.menuPopup.AppendSeparator()
        self.menuPopup.Append(self.idPopup[2], "Edit..")
        self.menuPopup.Append(self.idPopup[3], "Delete")
        # Events
        self.Bind(wx.EVT_LEFT_DOWN, self.evtLeftClick)
        self.Bind(wx.EVT_RIGHT_DOWN, self.evtRightClick)
        self.Bind(wx.EVT_PAINT, self.evtPaint)
        self.Bind(wx.EVT_SIZE, self.evtResize)

        self.idxSelected = None
        self.sequences = [
            Sequence([18, 31, 39, 51, 12, 5, 4])
        ]

    def _getParams(self, cntSequences=0):
        margin=8
        padding=3
        spacing=5
        height = 2 + padding + spacing*7
        offset = cntSequences * (height + margin) + margin
        return margin,padding,spacing,height,offset

    def recalcHeight(self):
        offset = self._getParams( len(self.sequences) )[4]
        self.SetVirtualSize( wx.Size(self.Size.width, offset) )

    def calcClickIdx(self, event):
        margin,_,_,height,_ = self._getParams(0)
        posX,posY = self.CalcUnscrolledPosition(event.GetX(),event.GetY())
        if posX < margin or posX > self.Size.width - margin or posY < margin:
            return None
        posY -= margin
        idx = int(posY // (height+margin))
        posY -= (height+margin) * idx
        if posY >= height:
            return None
        return idx;

    def drawMatrix(self, dc, idx, text, selected):
        margin,padding,spacing,height,offset = self._getParams(idx)
        width = self.Size.width - margin*2
        numCols = width // spacing - 1
        # Draw selection border (if any)
        if( selected ):
            dc.SetPen( self.penPx )
        else:
            dc.SetPen( wx.NullPen )
        dc.SetBrush( self.brushBG )
        dc.DrawRectangle(margin, offset, width, height)
        # Draw lit LEDs
        dc.SetPen( wx.NullPen )
        dc.SetBrush( self.brushPx )
        if text == []:
            # If no text, draw all LEDs lit
            for idxCol in range(0, numCols):
                for idxBit in range(0,7):
                    x = margin + padding + spacing * idxCol
                    y = offset + padding + spacing * idxBit
                    dc.DrawRectangle(x, y, 3, 3)
            return
        glyphs = self.app.glyphLookup
        symbol = glyphs[text[0]]
        idxLetter = 0
        idxCol = 0
        cntTotalCols = 0
        while cntTotalCols < numCols:
            if idxCol == symbol.width:
                idxLetter += 1
                if idxLetter == len(text):
                    break
                symbol = glyphs[text[idxLetter]]
                idxCol = 0
            bits = symbol.cols[idxCol]
            for idxBit in range(0,7):
                if bits & (1 << idxBit):
                    x = margin + padding + spacing * cntTotalCols
                    y = offset + padding + spacing * idxBit
                    dc.DrawRectangle(x, y, 3, 3)
            idxCol += 1
            cntTotalCols += 1

    def addSequence(self, text=None):
        newSequence = Sequence(text)
        self.idxSelected = len(self.sequences)
        self.sequences.append(newSequence)
        self.app.setActiveSequence(newSequence)
        self.recalcHeight()
        self.Refresh()

    def evtResize(self, event):
        self.recalcHeight()
        event.Skip()

    def evtPaint(self, event=None):
        dc = wx.PaintDC(self);
        self.DoPrepareDC(dc)
        for idx,seq in enumerate(self.sequences):
            self.drawMatrix(dc, idx, seq.text, self.idxSelected == idx)

    def evtLeftClick(self, event):
        self.idxSelected = self.calcClickIdx(event)
        if self.idxSelected is not None:
            if self.idxSelected >= len(self.sequences):
                self.idxSelected = None
        if self.idxSelected is None:
            self.app.setActiveSequence(None)
        else:
            self.app.setActiveSequence( self.sequences[self.idxSelected] )
        self.Refresh()

    def evtRightClick(self, event):
        idxSelected = self.calcClickIdx(event)
        if idxSelected is None or idxSelected >= len(self.sequences):
            return
        if True:
            # FIXME: Better with or without implicit select?
            self.idxSelected = idxSelected
            self.app.setActiveSequence( self.sequences[idxSelected] )
            self.Refresh()
        self.idxClick = idxSelected
        self.PopupMenu(self.menuPopup)

    def evtPopupUp(self, event):
        if self.idxClick == 0:
            return
        saved = self.sequences[self.idxClick]
        del self.sequences[self.idxClick]
        self.sequences.insert(self.idxClick-1, saved)
        self.idxSelected = self.idxClick-1
        self.Refresh()

    def evtPopupDown(self, event):
        if self.idxClick >= len(self.sequences):
            return
        saved = self.sequences[self.idxClick]
        del self.sequences[self.idxClick]
        self.sequences.insert(self.idxClick+1, saved)
        self.idxSelected = self.idxClick+1
        self.Refresh()

    def evtPopupEdit(self, event):
        self.app.dialogEdit.glyphs = self.app.glyphList
        self.app.dialogEdit.glyphLookup = self.app.glyphLookup
        self.app.dialogEdit.text = self.sequences[self.idxClick].copyText()
        result = self.app.dialogEdit.ShowModal()
        if result == wx.YES:
            self.sequences[self.idxClick].setText(self.app.dialogEdit.text)
            self.Refresh()
            self.app.hasChanges = True

    def evtPopupDel(self, event):
        del self.sequences[self.idxClick]
        self.idxSelected = None
        self.recalcHeight()
        self.app.setActiveSequence(None)
        self.Refresh()


class LEDApp(wx.App):
    ENTRY_SHOW=0
    ENTRY_RISE=1
    ENTRY_FALL=2
    ENTRY_LEFT=3
    EXIT_STOP=0
    EXIT_RISE=1
    EXIT_FALL=2
    EXIT_LEFT=3

    def OnInit(self):
        locale = wx.Locale();
        locale.Init(wx.LANGUAGE_DEFAULT)

        frame = wx.Frame(None)
        frame.app = self
        frame.SetInitialSize(wx.Size(640,480))

        menubar = wx.MenuBar()
        menuFile = wx.Menu()
        menuFile_New = menuFile.Append(
            wx.ID_ANY, "New", "")
        menuFile_Load = menuFile.Append(
            wx.ID_ANY, "Load..", "")
        menuFile_Save = menuFile.Append(
            wx.ID_ANY, "Save", "")
        menuFile_SaveAs = menuFile.Append(
            wx.ID_ANY, "Save As..", "")
        menuFile_ExportAsm = menuFile.Append(
            wx.ID_ANY, "Export sequence instructions (text)..", "")
        menuFile_ExportBin = menuFile.Append(
            wx.ID_ANY, "Export sequence instructions (binary)..", "")

        menuFile.AppendSeparator()
        menuFile_Glyphs = menuFile.Append(
            wx.ID_ANY, "&Edit symbol glyphs..", "Edit glyph bitmaps")
        menuFile_Add = menuFile.Append(wx.ID_ANY,
            "&Add symbol sequence..", "Add new glyph sequence")
        #menuFile_Auto = menuFile.Append(
        #    wx.ID_ANY, "Auto-play", kind=wx.ITEM_CHECK)
        #menuFile_Auto.Check(True)
        menuFile.AppendSeparator()
        menuFile_Exit = menuFile.Append(wx.ID_EXIT, "E&xit", "Terminate app")
        menubar.Append(menuFile, "&File")

        frame.Bind(wx.EVT_MENU, self.evtMenuNew, menuFile_New)
        frame.Bind(wx.EVT_MENU, self.evtMenuLoad, menuFile_Load)
        frame.Bind(wx.EVT_MENU, self.evtMenuSave, menuFile_Save)
        frame.Bind(wx.EVT_MENU, self.evtMenuSaveAs, menuFile_SaveAs)
        frame.Bind(wx.EVT_MENU, self.evtMenuExportAsm, menuFile_ExportAsm)
        frame.Bind(wx.EVT_MENU, self.evtMenuExportBin, menuFile_ExportBin)
        frame.Bind(wx.EVT_MENU, self.evtMenuGlyphs, menuFile_Glyphs)
        frame.Bind(wx.EVT_MENU, self.evtMenuAdd, menuFile_Add)
        frame.Bind(wx.EVT_MENU, self.evtMenuQuit, menuFile_Exit)
        frame.Bind(wx.EVT_CLOSE, self.evtQuit)

        frame.SetMenuBar(menubar)
        frame.SetTitle('wxLED')
        self.frameMain = frame
        xrc = wx.xrc.XmlResource('./wxLED.xrc')
        self.dialogFont = xrc.LoadDialog(self.frameMain,"dlgFont")
        self.dialogEdit = xrc.LoadDialog(self.frameMain,"dlgEdit")
        self.frameProps = xrc.LoadPanel(self.frameMain, "pnlProps")
        self.frameProps.Enable(False)
        self.frameProps.app = self

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        dataSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.pnlLED = MainDisplay(frame)
        self.pnlList = SequenceList(frame)
        dataSizer.Add(self.frameProps, 0, wx.EXPAND|wx.ALL, 0)
        dataSizer.Add(self.pnlList, 1, wx.EXPAND|wx.ALL, 0)

        mainSizer.Add(self.pnlLED, 0, wx.EXPAND|wx.ALL, 0 )
        mainSizer.Add(dataSizer,1, wx.EXPAND|wx.ALL, 0 )
        frame.SetSizer(mainSizer)
        frame.Show(True)
        self.loadDefaults()
        return True

    def loadDefaults(self):
        # Load default glyphs
        try:
            with open('symbols.json','rb') as fp:
                listSymbols = json.loads(fp.read())
                self.glyphList = []
                self.glyphLookup = {}
                for idx,(hotkey,pixels) in enumerate(listSymbols):
                    newGlyph = Glyph(pixels, hotkey, id=idx)
                    self.glyphList.append(newGlyph)
                    self.glyphLookup[idx] = newGlyph
        except IOError as e:
            wx.MessageBox(
                "Unable to load default symbols: {0}".format(e.strerror),
                "Error",
                wx.OK|wx.ICON_ERROR
                )
            self.glyphList = []
        self.hasChanges = False
        self.strFilename = None
        #FIXME: This needs to be configurable. Basing it on current window
        #       width caused headaches
        self.lenSequence = 60

    def loadFile(self, strFile):
        try:
            with open(strFile,'rb') as fp:
                data = json.loads(fp.read())
            self.glyphList = []
            self.glyphLookup = {}
            for dataGlyph in data['glyphs']:
                newGlyph = Glyph()
                newGlyph.setData(dataGlyph)
                self.glyphLookup[newGlyph.id] = newGlyph
                self.glyphList.append(newGlyph)
            self.pnlList.sequences = []
            for dataText in data['texts']:
                newSequence = Sequence()
                newSequence.setData(dataText)
                self.pnlList.sequences.append(newSequence)
        except IOError as e:
            wx.MessageBox(
                "Unable to load from '{0}' ({1})".format(strFile,e.strerror),
                "Error",
                wx.OK|wx.ICON_ERROR
                )
            self.glyphList = []
            self.pnlList.sequences = []
        self.setActiveSequence(None)
        self.pnlList.idxSelected = None
        self.pnlList.Refresh()

    def saveFile(self, strFile):
        self.frameProps.syncSeqWithCtrls()
        listGlyphs = []
        for glyph in self.glyphList:
            listGlyphs.append( glyph.getData() )
        listTexts = [seq.getData() for seq in self.pnlList.sequences]
        data = {
            'editor': 'wxLED',
            'glyphs': listGlyphs,
            'texts' : listTexts,
            }
        jsonUnicode = json.dumps(data)
        jsonBytes = jsonUnicode.encode('utf-8')
        try:
            with open(strFile,'wb') as fp:
                fp.write(jsonBytes)
        except IOError as e:
            wx.MessageBox(
                "Unable to save to '{0}' ({1})".format(strFile,e.strerror),
                "Error",
                wx.OK|wx.ICON_ERROR
                )

    def _getRowBits(self, listCols, idxRow):
        listBits = []
        for idxCol in range(0,len(listCols)):
            bit = (listCols[idxCol] >> idxRow) & 0x01
            if bit & 1:
                listBits.append( True )
            else:
                listBits.append( False )
        return listBits

    def exportFile(self, strFile, lenDisplay, exporter):
        self.frameProps.syncSeqWithCtrls()

        listGlyphs = []
        for glyph in self.glyphList:
            listGlyphs.append( glyph.getData() )
        listSeq = self.pnlList.sequences
        for seq in listSeq:
            isShort = False
            listCols = seq.getBits(self.glyphLookup)
            if False:
                print("entry:{0} wait:{1} hold:{2} mode:{3} wait:{4}".format(
                    seq.entry_mode, seq.entry_wait,
                    seq.exit_hold, seq.exit_mode, seq.exit_wait
                    ))

            listFrontCols = listCols[0:lenDisplay]
            # Technically not needed for rise/fall..
            if len(listFrontCols) < lenDisplay:
                listFrontCols.extend( [0] * (lenDisplay-len(listFrontCols)))

            # Sequence entry
            if seq.entry_mode == self.ENTRY_SHOW:
                exporter.instLoad( len(listFrontCols) )
                for col in listFrontCols:
                    exporter.instData(col)
                exporter.instDone()
            elif seq.entry_mode == self.ENTRY_RISE:
                for idxLine in range(0,7):
                    bits = self._getRowBits(listFrontCols, idxLine)
                    exporter.instRise(bits)
            elif seq.entry_mode == self.ENTRY_FALL:
                for idxLine in range(6,-1,-1):
                    bits = self._getRowBits(listFrontCols, idxLine)
                    exporter.instFall(bits)
            elif seq.entry_mode == self.ENTRY_LEFT:
                for col in listFrontCols:
                    exporter.instData(col)
                pass
            else:
                raise Exception("Unknown entry_mode {0}".format(seq.entry_mode))
            exporter.instWait(seq.entry_wait)

            # Middle columns (always scroll)
            listEndCols = listCols[lenDisplay:]
            for col in listEndCols:
                exporter.instData(col)
            exporter.instWait(seq.exit_hold)

            # Sequence exit
            if seq.exit_mode == self.EXIT_STOP:
                pass
            elif seq.exit_mode == self.EXIT_RISE:
                for idxLine in range(0,7):
                    exporter.instRise([False]*lenDisplay)
            elif seq.exit_mode == self.EXIT_FALL:
                for idxLine in range(6,-1,-1):
                    exporter.instFall([False]*lenDisplay)
            elif seq.exit_mode == self.EXIT_LEFT:
                for _ in range(0,lenDisplay):
                    exporter.instData(0)
                pass
            else:
                raise Exception("Unknown exit_mode {0}".format(seq.exit_mode))
            exporter.instWait(seq.exit_wait)
        try:
            with open(strFile,'wb') as fp:
                exporter.write(fp)
        except IOError as e:
            wx.MessageBox(
                "Error exporting to '{0}' ({1})".format(strFile,e.strerror),
                "Error",
               wx.OK|wx.ICON_ERROR
                )

    def askKeepChanges(self):
        reply = wx.MessageBox(
            "Save changes before exiting?",
            "Save changes",
            wx.YES_NO|wx.CANCEL|wx.CANCEL_DEFAULT
            )
        if reply == wx.CANCEL:
            return False
        elif reply == wx.YES:
            self.evtMenuSave(None)
        return True

    def evtMenuNew(self, event):
        if self.hasChanges and not self.askKeepChanges():
            return
        self.pnlList.sequences = []
        self.pnlList.idxSelected = None
        self.pnlList.recalcHeight()
        self.setActiveSequence(None)
        self.pnlList.Refresh()
        self.loadDefaults()

    def evtMenuLoad(self, event):
        if self.hasChanges and not self.askKeepChanges():
            return
        with wx.FileDialog(
                self.frameMain, "Open sequence file",
                wildcard="Sequence files (*.json)|*.json",
                style=wx.FD_OPEN|wx.FD_FILE_MUST_EXIST) as dialogFile:
            if dialogFile.ShowModal() == wx.ID_CANCEL:
                return
            self.loadFile(dialogFile.GetPath())
            self.hasChanges = False

    def evtMenuSave(self, event):
        if self.strFilename is not None:
            self.saveFile(self.strFilename)
            self.hasChanges = False
        else:
            self.evtMenuSaveAs(event)

    def evtMenuSaveAs(self, event):
        with wx.FileDialog(
                self.frameMain, "Save sequence file",
                wildcard="Sequence files (*.json)|*.json",
                style=wx.FD_SAVE|wx.FD_OVERWRITE_PROMPT) as dialogFile:
            if dialogFile.ShowModal() == wx.ID_CANCEL:
                return
            self.strFilename = dialogFile.GetPath()
            self.saveFile(self.strFilename)
            self.hasChanges = False


    def evtMenuExportAsm(self, event):
        with wx.FileDialog(
            self.frameMain,
            "Export display instructions (text)",
            defaultFile='export',
            wildcard="Display instruction files (*.led)|*.led|All files (*.*)|*.*",
            style=wx.FD_SAVE|wx.FD_OVERWRITE_PROMPT) as dialogFile:
            if dialogFile.ShowModal() == wx.ID_CANCEL:
                return
            strFilename = dialogFile.GetPath()
            self.exportFile(strFilename, self.lenSequence, ExporterAsm())

    def evtMenuExportBin(self, event):
        with wx.FileDialog(
            self.frameMain,
            "Export display instructions (binary)",
            defaultFile='export',
            wildcard="Display instruction binary (*.bin)|*.bin|All files (*.*)|*.*",
            style=wx.FD_SAVE|wx.FD_OVERWRITE_PROMPT) as dialogFile:
            if dialogFile.ShowModal() == wx.ID_CANCEL:
                return
            strFilename = dialogFile.GetPath()
            self.exportFile(strFilename, self.lenSequence, ExporterBin())

    def evtQuit(self, event):
        # Unsetting active sequence flushes changes to properties
        activeSeq = self.frameProps.activeSeq
        self.frameProps.setActiveSequence(None)
        if not self.hasChanges:
            event.Skip()
        elif self.askKeepChanges():
            event.Skip()
        else:
            event.Veto()
            self.frameProps.setActiveSequence(activeSeq)

    def evtMenuQuit(self, event):
        self.frameMain.Close()

    def evtMenuGlyphs(self, event):
        setUsed = set()
        for idx,seq in enumerate(self.pnlList.sequences):
            setUsed = setUsed.union(seq.used)
        listGlyphs = copy.deepcopy(self.glyphList)
        for glyph in listGlyphs:
            if glyph.id in setUsed:
                glyph.inUse = True
            else:
                glyph.inUse = False
        self.dialogFont.glyphs = listGlyphs
        maxId = -1
        for glyph in self.dialogFont.glyphs:
            if glyph.id is not None and glyph.id >= maxId:
                maxId = glyph.id
        self.dialogFont.nextId = maxId + 1
        result = self.dialogFont.ShowModal()
        if result == wx.YES:
            self.glyphLookup = {}
            for glyph in self.dialogFont.glyphs:
                self.glyphLookup[ glyph.id ] = glyph
            self.glyphList = self.dialogFont.glyphs
            # Force refresh so other panels use updated glyphs
            self.pnlList.Refresh()
            self.hasChanges = True

    def evtMenuAdd(self, event):
        self.dialogEdit.glyphs = self.glyphList
        self.dialogEdit.glyphLookup = self.glyphLookup
        self.dialogEdit.text = []
        result = self.dialogEdit.ShowModal()
        if result == wx.YES:
            self.pnlList.addSequence(self.dialogEdit.text)
            self.hasChanges = True

    def setActiveSequence(self, newSelection):
        self.frameProps.setActiveSequence(newSelection)


if __name__ == "__main__":
    app = LEDApp()
    app.MainLoop()
